﻿using ManhattanDistance.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManhattanDistance.Data
{
    public class EventRepository
    {
        /// <summary>
        /// Generates a list of random events.
        /// </summary>
        /// <param name="number">The number of events to be generated</param>
        /// <returns>A list of randomly generated events as a list.</returns>
        public List<Event> GetAllEvents(int number)
        {
            List<Event> allEvents = new List<Event>();
            Random randomEventId = new Random();
            for (int i = 0; i < number; i++)
            {
                var evnt = new Event();
                evnt = GenerateFake(randomEventId.Next(0, 100), randomEventId);
                allEvents.Add(evnt);
            }
            return allEvents;
        }

        /// <summary>
        /// Gennerate fake event object.
        /// </summary>
        /// <param name="number">A random number.</param>
        /// <param name="r">The random object to create a random number</param>
        /// <returns>The fake event object.</returns>
        private Event GenerateFake(int number, Random r)
        {
            Event fake = new Event()
            {
                EventId = number,
                Tickets = fakeTickets(number),
                XCoordinate = r.Next(-10, 10),
                YCoordinate = r.Next(-10, 10)
            };
            return fake;
        }

        /// <summary>
        /// Creates a list of fake tickets.
        /// </summary>
        /// <param name="number">A random number.</param>
        /// <returns>A list of fake event tickets as list.</returns>
        private List<Ticket> fakeTickets(int number)
        {
            List<Ticket> tickets = new List<Ticket>();
            Ticket ticket1 = new Ticket()
            {
                TicketDescription = "Unrestricted View",
                TicketName = "Section 330",
                TicketPrice = number * 10
            };
            tickets.Add(ticket1);
            Ticket ticket2 = new Ticket()
            {
                TicketDescription = "Unrestricted View",
                TicketName = "Section 350",
                TicketPrice = number * 5
            };
            tickets.Add(ticket2);
            Ticket ticket3 = new Ticket()
            {
                TicketDescription = "Unrestricted View",
                TicketName = "Section 370",
                TicketPrice = number * 2
            };
            tickets.Add(ticket3);
            return tickets;
        }
    }
}
