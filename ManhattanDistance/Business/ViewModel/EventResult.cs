﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManhattanDistance.Business.ViewModel
{
    /// <summary>
    /// A view model to represent the required result.
    /// </summary>
    public class EventResult
    {
        /// <summary>
        /// The unique identifier of the event.
        /// </summary>
        public int EventId { get; set; }

        /// <summary>
        /// The distance from the location.
        /// </summary>
        public int Distance { get; set; }

        /// <summary>
        /// The lowest ticket price in the 
        /// </summary>
        public decimal LowestTktPrice { get; set; }

        /// <summary>
        /// The XCoordinate of event.
        /// </summary>
        public int XCoordinate { get; set; }

        /// <summary>
        /// The Y Coordinate of event.
        /// </summary>
        public int YCoordinate { get; set; }
    }
}
