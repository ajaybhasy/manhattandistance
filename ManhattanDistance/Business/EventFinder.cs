﻿using ManhattanDistance.Business.ViewModel;
using ManhattanDistance.Data;
using ManhattanDistance.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManhattanDistance.Business
{
    public class EventFinder
    {
        /// <summary>
        /// Gets the nearest five locations of events with the lowest ticket price.
        /// </summary>
        /// <param name="x">The x Cordninate of the Location.</param>
        /// <param name="y">The y coordniate of the location.</param>
        /// <param name="numberOfEvents"></param>
        /// <returns>The list of 5 nearest location</returns>
        public Tuple<List<EventResult>, List<Event>> GetNearestFive(int x, int y, int numberOfEvents)
        {
            var events = GetAllEvents(numberOfEvents);
            List<EventResult> results = new List<EventResult>();
            foreach (var evnt in events)
            {
                decimal lowestPrice = evnt.Tickets.Count > 0 ? evnt.Tickets.OrderBy(p => p.TicketPrice).FirstOrDefault().TicketPrice : 0;
                int dist = GetManhattanDistance(x, y, evnt.XCoordinate, evnt.YCoordinate);
                EventResult result = new EventResult()
                {
                    Distance = dist,
                    EventId = evnt.EventId,
                    LowestTktPrice = lowestPrice,
                    XCoordinate = evnt.XCoordinate,
                    YCoordinate = evnt.YCoordinate
                };
                results.Add(result);

            }
            return Tuple.Create(results.OrderBy(p => p.Distance).Skip(0).Take(5).ToList(), events);
        }

        /// <summary>
        /// Gets the list of all randomly generated events.
        /// </summary>
        /// <param name="number">The number of events to be generated.</param>
        /// <returns></returns>
        private List<Event> GetAllEvents(int number)
        {
            EventRepository e = new EventRepository();
            var events = e.GetAllEvents(number);
            return events;
        }

        /// <summary>
        /// Gets the manhattan distance between two points.
        /// </summary>
        /// <param name="x1">XCoordinate of first point</param>
        /// <param name="y1">YCoordinate of first point</param>
        /// <param name="x2">XCordinate of second point</param>
        /// <param name="y2">YCordinate of second point</param>
        /// <returns>The distance beteween the two points</returns>
        private int GetManhattanDistance(int x1, int y1, int x2, int y2)
        {
            return Math.Abs(x1 - x2) + Math.Abs(y1 - y2);
        }

        public void GetNearestFiveinLargerWorld(int x, int y)
        {
            var events = GetAllEvents(10);
            var xCoords = events.Select(p => p.XCoordinate).ToList();
            xCoords.Add(x);
            var yCoords = events.Select(p => p.YCoordinate).ToList();
            yCoords.Add(y);
            xCoords.Sort();
            yCoords.Sort();
            int xpos = xCoords.FindIndex(p => p == x);
            int ypos = yCoords.FindIndex(p => p == y);
            List<int> x5 = new List<int>();
            List<int> y5 = new List<int>();

            for (int i = -3; i <= 3; i++)
            {
                if (xpos + i < 11)
                {
                    x5.Add(xCoords[xpos + i]);
                }
                if (ypos + i < 11)
                {
                    y5.Add(yCoords[ypos + i]);
                }
            }
            var firstPointx = xCoords.Where(p => xCoords[xpos - 1] <= p && p <= xCoords[xpos + 1]).ToList();
            var firstpointy = yCoords.Where(p => yCoords[ypos - 1] <= p && p <= yCoords[ypos + 1]).ToList();
            //x5.Sort();
            //y5.Sort();

            //int[] x5 = new int[] { xCoords[xpos - 2], xCoords[xpos - 1], xCoords[xpos], xCoords[xpos + 1], xCoords[xpos + 2] };
            //int[] y5 = new int[] { yCoords[ypos - 2], yCoords[ypos - 1], yCoords[ypos], yCoords[ypos + 1], yCoords[ypos + 2] };
            //int x0 = xCoords[2];
            //int x1 = xCoords[3];
            //int y0 = yCoords[2];
            //int y1 = yCoords[3];
        }
    }
}
