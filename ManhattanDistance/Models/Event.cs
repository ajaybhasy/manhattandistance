﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManhattanDistance.Models
{
    /// <summary>
    /// The Event model representing an event in the real world.
    /// </summary>
    public class Event
    {
        /// <summary>
        /// The unique identifier for the event.
        /// </summary>
        public int EventId { get; set; }

        /// <summary>
        /// The xcoordinate of the event location.
        /// </summary>
        public int XCoordinate { get; set; }

        /// <summary>
        /// The Y coordinate of the event location.
        /// </summary>
        public int YCoordinate { get; set; }

        /// <summary>
        /// The list of tickets for the event.
        /// </summary>
        public List<Ticket> Tickets { get; set; }
    }
}
