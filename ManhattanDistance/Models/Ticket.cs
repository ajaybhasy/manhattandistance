﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManhattanDistance.Models
{
    /// <summary>
    /// A Ticket model representing the real world Ticket.
    /// </summary>
    public class Ticket
    {
        /// <summary>
        /// The name of ticket.
        /// </summary>
        public string TicketName { get; set; }

        /// <summary>
        /// The price of the ticket.
        /// </summary>
        public decimal TicketPrice { get; set; }

        /// <summary>
        /// A short description about the ticket.
        /// </summary>
        public string TicketDescription { get; set; }
    }
}
