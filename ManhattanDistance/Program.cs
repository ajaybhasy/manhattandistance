﻿using ManhattanDistance.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManhattanDistance
{
    class Program
    {
        static void Main(string[] args)
        {
            EventFinder f = new EventFinder();

            //A more effective way to calculate the events that are nearest if the world is big or number of events are huge.(Not Complete)
            //f.GetNearestFiveinLargerWorld(3,4);


            Console.WriteLine("Please input Coordinates:");

            // To find the X and Y coordinates from the console.
            string coord = Console.ReadLine();
            string[] xy = coord.Split(',');
            int x = Convert.ToInt32(xy[0]);
            int y = Convert.ToInt16(xy[1]);

            // Gets the results (Edit the last parameter to increase or decrease the total number of events)
            var results = f.GetNearestFive(x, y, 10);

            //Prints the result in required format.
            foreach (var res in results.Item1)
            {
                Console.WriteLine(string.Format("Event {0} ({1},{2}) - ${3}, Distance {4}", res.EventId, res.XCoordinate, res.YCoordinate, res.LowestTktPrice, res.Distance));
            }

            // Prints the total number of events (Uncomment to view the total number of events)
            //Console.WriteLine();
            //Console.WriteLine("The total events are:");
            //foreach (var evnt in results.Item2)
            //{
            //    Console.WriteLine(string.Format("Event {0} ({1},{2})", evnt.EventId, evnt.XCoordinate, evnt.YCoordinate));
            //}
            Console.Read();
        }
    }
}
